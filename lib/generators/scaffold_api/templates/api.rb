module <%= api_version.upcase %>
  class <%= api_class_name %> < Base

    resources :<%= plural_table_name %> do

      desc "Список ###<%= api_class_name %>###"
      get '/' do
        @<%= plural_table_name %> = <%= orm_class.all(class_name) %>
      end

    end

  end
end
