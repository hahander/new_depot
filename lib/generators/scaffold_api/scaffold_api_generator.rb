class Rails::ScaffoldApiGenerator < Rails::Generators::NamedBase
  include ::Rails::Generators::ResourceHelpers
  source_root File.expand_path('../templates', __FILE__)
  argument :version, type: :string, default: "v1"

  class_option :orm, :banner => "NAME", :type => :string, :required => true,
                     :desc => "ORM to generate the api for"

  def generate_api_controller
    template 'api.rb', File.join('app', 'api', api_version, class_path, "#{api_file_name}.rb")
    mount_api "mount #{api_version.upcase}::#{api_class_name}"
  end

  def generate_api_views
    empty_directory File.join('app', 'views', 'api', api_file_name)
    viewname = 'index.rabl'
    template viewname, File.join('app', 'views', 'api', api_file_name, viewname)
  end

protected

  alias_method :api_name, :controller_name
  alias_method :api_file_name, :controller_file_name
  alias_method :api_class_name, :controller_class_name

  def api_version
    version.underscore
  end

  def mount_api(mount_code)
    log "Add '#{mount_code}' to app/api/#{api_version}/all.rb"
    inject_into_file(File.join('app', 'api', api_version, "all.rb"), {after: /version '#{api_version}' do\s*$/, verbose: false}) do
      "\n      #{mount_code}"
    end
  end

end
