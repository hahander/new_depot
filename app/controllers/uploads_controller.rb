class UploadsController < ApplicationController
  load_and_authorize_resource
  respond_to :html

  before_action :set_upload, only: [:show, :edit, :update, :destroy]
  
  def index

  end

  def show

  end

  def new
    @upload = Upload.new
  end

  def edit

  end

  def create
    @upload.save
  end

  def update
    @upload.update(upload_params)
  end

  def destroy
    @upload.destroy
  end

  private
    def set_upload
      @product = Upload.find(params[:id])
    end

    def upload_params
      params.require(:upload).permit(:upload)
    end
end