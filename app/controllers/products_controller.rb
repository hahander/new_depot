class ProductsController < ApplicationController
  load_and_authorize_resource
  skip_load_resource only: [:create]
  respond_to :html
  before_action :set_product, only: [:show, :edit, :update, :destroy]

  def index
    @products = @products.includes(:uploads).page(params[:page])
  end

  def show

  end

  def new
    @product.uploads.build
  end

  def edit
  end

  def create
    @product = Product.new(product_params)
    redirect_to @product if @product.save
  end

  def update
    @product.update(product_params)
    render :show
  end

  def destroy
    @product.destroy
    redirect_to products_path
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_product
      @product = Product.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def product_params
      params.require(:product).permit(:title, :description, :price, { uploads_attributes: [:upload] } )
    end
end
