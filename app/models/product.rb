class Product < ActiveRecord::Base
  has_many :uploads
  accepts_nested_attributes_for :uploads, reject_if: :all_blank
end
