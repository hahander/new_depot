class Upload < ActiveRecord::Base
  belongs_to :product
  has_attached_file :upload, styles: { medium: "300x300>", thumb: "100x100>" },
                             path: ":rails_root/public/system/:class/:style/:filename",
                             url: "/system/:class/:style/:filename"
  validates_attachment_content_type :upload, :content_type => /\Aimage\/.*\Z/
end
