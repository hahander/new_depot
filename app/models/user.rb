class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  enum role: [:admin, :moderator, :user]
  devise :database_authenticatable, :registerable,
         #:recoverable, :trackable 
         :rememberable, :validatable
end
