class UploadInput < SimpleForm::Inputs::FileInput
  def input
    out = '' # the output string we're going to build
    # check if there's an uploaded file (eg: edit mode or form not saved)
    if object.send("#{attribute_name}?")
      # append preview image to output
      # <%= image_tag @user.upload.url(:thumb), :class => 'thumbnail', id: 'upload' %>
      out << template.image_tag(object.send(attribute_name).url(:thumb), :class => 'thumbnail', id: 'upload')
    end
    # append file input. it will work accordingly with your simple_form wrappers
    (out << @builder.file_field(attribute_name, input_html_options)).html_safe
    out << template.image_tag('')
  end
 end
