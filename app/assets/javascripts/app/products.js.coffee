#= require_self
#= require './resources'
#= require_tree './controllers'

app = angular.module('new_depot.app.products', [
  'new_depot.app.config', 'ngResource', 'new_depot.app.resources', 'ngSanitize', 'ngTable'
])
