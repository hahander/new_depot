angular.module('new_depot.app.products').controller 'ProductsCtrl', ["$scope", "Product", 'ngTableParams', ($scope, Product, ngTableParams) ->

  $scope.tableParams = new ngTableParams(
    page: 1 # show first page
    count: 10 # count per page
  ,
    total: 0
    getData: ($defer, params) ->
      Product.query params.page(), (data) ->
        params.total data.total
        $defer.resolve(data.products.slice((params.page() - 1) * params.count(), params.page() * params.count()))
        console.log data.user
  )

]
