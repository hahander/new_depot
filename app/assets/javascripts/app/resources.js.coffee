app = angular.module('new_depot.app.resources', [])

app.config ["$httpProvider", ($httpProvider) ->
  getToken = ->
    el = document.querySelector("meta[name=\"csrf-token\"]")
    el = el.getAttribute("content")

  updateToken = ->
    headers = $httpProvider.defaults.headers.common
    token = getToken()
    if token
      headers["X-CSRF-TOKEN"] = getToken
      headers["X-Requested-With"] = "XMLHttpRequest"
    return

  updateToken()
  $(document).bind "page:change", updateToken  if window["Turbolinks"]
]

app.factory 'Product', ['$resource', ($resource) ->
  $resource '/api/v1/products/:id.json',
    id: '@id'
    title: '@title'
    description: '@description'
    price: '@price'
  ,
    save: { method: 'POST' }
    query: { method: 'GET', isArray: false }
]

app.factory 'Upload', ['$resource', ($resource) ->
  $resource 'api/v1/uploads/:id.json',
    id: '@id'
    product_id: '@product_id'
    upload: '@upload'
  ,
    save: { method: 'POST' }
]
