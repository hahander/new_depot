# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.

$('form#sign_in_user').bind 'ajax:success', (e, data, status, xhr) ->
  if data.success
    $.ajax(
      url: 'users/sessions/_new.html.slim'
      cache: false)
  else
    $('div.alert_inner').html("<div class='login_alert alert alert-error'>
                                  <button class='close' type='button' data-dismiss='alert'>×</button>
                                  Invalid login or password!
                                </div>")
    $('div.login_alert').fadeIn() 