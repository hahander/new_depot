# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.

readURL = (input, object) ->
  if input.files and input.files[0]
    reader = new FileReader()
    reader.onload = (e) ->
      object.children('img').attr "src", e.target.result
      return

    reader.readAsDataURL input.files[0]
  return

$ ->
  $('input:file').each ->
      $(this).on
        change: ->
          readURL(this, $(this).parent('.file_upload'))
          return

  $('span.add_field').click ->
    count = $('input:file').length
    string = "<div class='file_upload'>  
                <div class='control-group upload optional product_uploads_upload'>
                  <label class='upload optional control-label' for='product_uploads_attributes_0_upload'>Upload</label>
                  <div class='controls'>  
                    <input class='file_select' id='product_uploads_upload' name='product[uploads_attributes]["+count+"][upload]' type='file'>
                    <img src=''>
                  </div>
                </div>
              </div>";
    $('.file_upload_area').append(string)
    $('input:file').each ->
      $(this).on
        change: ->
          readURL(this, $(this).parent('.controls'))
          return
