module Depot
  class API < Grape::API

    format :json
    formatter :json, Grape::Formatter::Rabl
    content_type :json, "application/json; charset=utf-8"

    rescue_from Grape::Exceptions::ValidationErrors do |e|
      Rack::Response.new({
        error: "validation_error",
        message: e.message
      }.to_json, e.status, 'Content-Type' => 'application/json; charset=utf-8')
    end

    rescue_from ActiveRecord::RecordNotFound do |e|
      Rack::Response.new({
        error: "general_error",
        message: I18n.t("errors.general_errors.record_not_found")
      }.to_json, 404, 'Content-Type' => 'application/json; charset=utf-8')
    end

    rescue_from ActiveRecord::RecordInvalid do |e|
      Rack::Response.new({
        error: "validation_error",
        message: e.message
      }.to_json, 400, 'Content-Type' => 'application/json; charset=utf-8')
    end

    mount V1::All

  end
end
