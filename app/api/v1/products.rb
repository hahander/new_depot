module V1
  class Products < Base

    resources :products do

      desc "Список ###Products###"
      get '/', rabl: 'products/index' do
        @products = Product.includes(:uploads)
        @user = current_user
      end

      desc "Add product"
      params do
        requires :title, type: String, desc: 'Title'
        requires :description, type: String, desc: 'Description'
        requires :price, type: String, desc: 'Price'
      end
      post '/', rabl: 'product/ok' do
        Product.create!(title: params[:title], description: params[:description], price: params[:price])
      end

    end

  end
end
