module V1
  class All < Grape::API

    helpers ::Helpers::SessionHelper

    version 'v1' do
      mount V1::Uploads
      mount V1::Products
    end

  end
end
