module V1
  class Uploads < Base

    resources :uploads do

      desc "Add upload"
      params do
        requires :product_id, type: Integer, desc: 'Product ID'
        requires :image, desc: 'Image file'
      end
      post '/', rabl: 'uploads/ok' do
        Product.find(params[:product_id]).uploads.create(upload: params[:image])
      end

    end

  end
end
