module V1
  class Base < Grape::API

    # helper for swagger documentation
    def self.several_notes(*notes)
      notes.join("\n")
    end

#     # helper for swagger documentation
#     def self.response_example(template, obj_name, obj_value)
#       <<-NOTE
# Response example
# -----------------
# ```json
# #{JSON.pretty_generate JSON.parse(Rabl::Renderer.new(template,
#                                                      instance_variable_set("@#{obj_name}", obj_value),
#                                                      :locals => { obj_name => obj_value },
#                                                      :format => :json).render)}
# ```
#       NOTE
#     end

  end
end
