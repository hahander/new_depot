module Helpers
  module SessionHelper

    def error!(message, status)
      super(message, status, 'Content-Type' => 'application/json; charset=utf8')
    end

    module Errors
      SESSION_EXPIRED = 'Session expired'
      TOKEN_MISSED = 'Token missed'
    end

    def token
      headers['Authorization'] || params['token']
    end

    def check_token!
      error!(Errors::TOKEN_MISSED, 401) if (token.nil? && env['warden'].user.nil?)
    end

    def session!
      @session ||= Session.includes(:user).find_by_token(token)
      error!({ :error => Errors::SESSION_EXPIRED, :message => "Сессия истекла"}, 401) if !@session || @session.expired?
      @session
    end

    def current_user
      @current_user ||= session!.user if token
      @current_user ||= env['warden'].user
    end

    def check_private_resource!
      check_token!
    end
  end
end
