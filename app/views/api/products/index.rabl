child @products, root: :products, object_root: false do
  attributes :id, :title, :description, :created_at
  child(:uploads, object_root: false) { attributes :id, :upload }
end
node(:total) { @products.count }
node(:user) { @current_user }
