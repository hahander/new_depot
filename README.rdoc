== README

== Instaling Ruby 

sudo apt-get update

* sudo apt-get install git-core curl zlib1g-dev build-essential libssl-dev libreadline-dev libyaml-dev libsqlite3-dev sqlite3 libxml2-dev libxslt1-dev libcurl4-openssl-dev python-software-properties
* sudo apt-get install libgdbm-dev libncurses5-dev automake libtool bison libffi-dev
* curl -L https://get.rvm.io | bash -s stable
* source ~/.rvm/scripts/rvm
* echo "source ~/.rvm/scripts/rvm" >> ~/.bashrc [or .zshrc]
* rvm install 2.1.2
* rvm use 2.1.2 --default
* ruby -v

Do not install docs:
* echo "gem: --no-ri --no-rdoc" > ~/.gemrc

== Instaling Rails

* gem install rails

JSruntime:
* sudo apt-get install nodejs

== Setup PostgreSQL

*sudo sh -c "echo 'deb http://apt.postgresql.org/pub/repos/apt/ precise-pgdg main' > /etc/apt/sources.list.d/pgdg.list"
*wget --quiet -O - http://apt.postgresql.org/pub/repos/apt/ACCC4CF8.asc | sudo apt-key add -
*sudo apt-get update
*sudo apt-get install postgresql-common
*sudo apt-get install postgresql-9.3 libpq-dev

= Configure postresql

* sudo -u postgres psql
* ALTER ROLE user_name WITH ENCRYPTED PASSWORD 'user_password';
* \q

* nano /etc/postgresql/{#version}/main/pg_hba.conf
and setup md5 encoding
