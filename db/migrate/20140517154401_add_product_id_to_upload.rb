class AddProductIdToUpload < ActiveRecord::Migration
  def change
    add_column :uploads, :product_id, :integer
  end
end
