# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
# 1000.times do
#   product = Product.create!(title: 'asd', description: '123332', price: 112)
#   product.uploads.create!(upload: File.open(Rails.root.join('app', 'assets', 'images', 'default_photo_image_original.png')))
# end
